from app import return_bbsort ,return_quick_sort_algorithm,return_merge_sort_algorithm
from array import array
from flask import Flask, json ,render_template,jsonify,request

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/shot/<n>',methods=['GET'])
def shorteD(n):
    x= array('i', list(dict.fromkeys([int(x) for x in (n.split(','))])))
    v = list(return_merge_sort_algorithm(x))
    json_d = jsonify({'array':v})
    return json_d

if __name__ == '__main__':
    app.run(debug=0)
