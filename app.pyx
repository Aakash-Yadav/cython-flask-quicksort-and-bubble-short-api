from cpython cimport array

cdef bubble_sort(array.array data,int length):
    cdef int true = 1
    cdef int index,found
    while true:
        found = 0 
        for index in range(length-1):
            if data[index] > data[index+1]:
                data[index] , data[index+1] = data[index+1],data[index]
                found = 1 
        if found == 0:
            return data 

cdef quick_sort_algorithm(array.array given_array):
    cdef int end_value,x
    if not (given_array) :
        return given_array 
    else:
        end_value = given_array[-1]
        return (quick_sort_algorithm(array.array('i',[x for x in given_array if x<end_value]))+
            array.array('i',[end_value])+
            quick_sort_algorithm(array.array('i',[x for x in given_array if x>end_value])))



############################ Merge Sort Algorithm

cdef merge_sort_algorithm(array.array left, array.array right ):
    cdef int i , j 
    cdef array.array result = array.array('i',[])
    i,j = 0,0 
    while len(left)>i and len(right)>j:
        if left[i] <= right[j]:
            result.append(left[i])
            i = i+1
        else:
            result.append(right[j])
            j = j+1    
    result += left[i:] + right[j:]
    return result


cdef merge_sort_algorithm_exc(array.array data):
    cdef int mid 
    cdef array.array left,right 
    if len(data)<=1:
        return data
    mid = len(data)//2
    left = merge_sort_algorithm_exc(data[:mid])
    right = merge_sort_algorithm_exc(data[mid:])
    return merge_sort_algorithm(left,right)

#                                                    SELECTION 
cdef selection_sort(array.array data):
    cdef int i , Min
    for i in range(len(data)):
        Min =  data[ min(data[i:]) ]    
        data[i] , data[Min] = data[Min],data[i]
    return data

#                                        insertion
cdef insertion_sort(array.array data):
    cdef int i , j 
    for i in range(1,len(data)):
        j = i 
        while data[j-1]>data[j] and j>0:
            data[j-1],data[j] = data[j],data[j-1]
            j = j-1
    return data 

#################################################################################
##################################### RETURN C++ FUN ############################
################################################################################

cpdef return_bbsort(array.array data,int length):
    return bubble_sort(data,length)

cpdef return_quick_sort_algorithm(array.array data):
    return quick_sort_algorithm(data)

cpdef return_merge_sort_algorithm(array.array x):
    return merge_sort_algorithm_exc(x)

cpdef return_insertion_sort(array.array x):
    return insertion_sort(x)

cpdef return_selection_sort(array.array x):
    return selection_sort(x)
