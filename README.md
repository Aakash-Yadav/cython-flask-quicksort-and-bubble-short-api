
## Cython To Python (API)
![alt text](https://xp.io/storage/2OSjU63T.png)
### requirements :-
- Python3.6 +
- Flask
- Cython

### (1) Cython :
 Cython is a programming language that makes writing C extensions for the Python language as easy as Python itself. It aims to become a superset of the [Python] language which gives it high-level, object-oriented, functional, and dynamic programming. Its main feature on top of these is support for optional static type declarations as part of the language. The source code gets translated into optimized C/C++ code and compiled as Python extension modules. This allows for both very fast program execution and tight integration with external C libraries, while keeping up the high programmer productivity for which the Python language is well known.
 
 ### Fast Code
 ![alt text](https://xp.io/storage/2OSHmqtn.png)
 
 ### (2) Flask :
 ![alt text](https://xp.io/storage/2OTA0zR2.png)
 #### ....
 Flask is a micro web framework written in Python. It is classified as a microframework because it does not require particular tools or libraries. It has no database abstraction layer, form validation, or any other components where pre-existing third-party libraries provide common functions.
 
### (3) about project 
using Flask and Cython created sorting algorithms quick sort , Bubble sort

1. #### Quick-sort
### What is the Quicksort Algorithm?
![alt text](https://xp.io/storage/2OT9FVqy.png)

The Quicksort algorithm is a systematic routine for sorting elements of an array. It is efficient when compared to other common sorting algorithms, and it is considered unstable because the relative order of equal elements is not guaranteed. The name "Quicksort" refers to the fact that this algorithm is capable of sorting data much

### How does the Quicksort Algorithm work?
The algorithm works by partitioning the array into two smaller sub-arrays with smaller values on one side and larger on the other. This partitioning can then be recursively applied to the sub-arrays to form even smaller partitioned sub-arrays. The base case for the recursion is a sub-array of size one or size zero, which are already sorted by definition. Like the sorting algorithm Merge sort, Quicksort takes the divide and conquer approach to processing data. 
### Quicksort Complexity
When choosing a sorting algorithm, efficiency is often one of the most important factors. Below are some of the efficiency characteristics of the Quicksort algorithm:

### Algorithmic Time Complexity -
In the best, average, and worst cases, the Quicksort algorithm performs with O(n), O(n log n), and O(n^2) complexities, respectively. It is one of the most efficient sorting algorithms when it comes to time complexity.

### Algorithmic Space Complexity - 
It’s average space complexity is O(log n) and worst case space complexity is O(n). This is on par with most of the popular sorting algorithms, but the nature of recursive algorithms is that they do not optimize around memory usage.

### Optimizations -
With any algorithm, there are often some optimizations that can be applied above and beyond the general use cases. To make sure that the used space is limited to O(log n), the algorithm can be implemented to recurse into the smaller side of the partition and also use tail recursion. One may also implement a hybrid sorting algorithm that switches to an iterative sorting algorithm that may be more time-efficient for smaller arrays.


2. ### Bubble short 

## what is the Bubble short Algorithm?
![alt text](https://xp.io/storage/2OTMoVJb.png)
### ...
Bubble sort is one of the fundamental forms of sorting in programming. Bubble sort algorithms move through a sequence of data (typically integers) and rearrange them into ascending or descending order one number at a time. To do this, the algorithm compares number X to the adjacent number Y. If X is higher than Y, the two are swapped and the algorithm starts over.]

This process repeats until the entire range of numbers has been sorted in the desired order. For instance, if you were trying to arrange [1, 3, 2, 4] into ascending order, the bubble sort algorithm would run once, swapping the 3 with the 2. 

In another matrix, however, your numbers might look like this: [3, 1, 4, 2]. In this case, the algorithm would run three times, swapping the 3 and the 1 the first time, then the 4 and the 2 the second time, and finally the 3 and the 2. 
## What does bubble sort mean?
Bubble sort's strong point is its simplicity. It takes just a few lines of code, is easy to read, and can be plugged in anywhere in your program. However, it's extremely inefficient for larger sets of numbers and should be used accordingly.

## Time Complexity Analysis
In this unoptimised version the run time complexity is Θ(N^2). This applies to all the cases including the worst, best and average cases because even if the array is already sorted the algorithm doesn't check that at any point and runs through all iterations. Although the number of swaps would differ in each case.

### WEB APP:
## Random
![alt text](https://xp.io/storage/2OU65NZu.png)
## SORT-By(Quicksort)
![alt text](https://xp.io/storage/2OU0tlTv.png)

#### thank you :)



    

